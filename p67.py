import os
os.chdir(os.path.expanduser("~") + "/src/project-euler")
tri = open('p067_triangle.txt', 'r')
List = [[int(y) for y in x.split()] for x in tri.readlines()]
n = len(List)
m = len(List[n-1])
sums = [max(List[n-1][j], List[n-1][j+1]) for j in range(m-1)]
for i in range(n-2, 0, -1):
    print(sums)
    M = len(List[i])
    new_sums = [List[i][j] + sums[j] for j in range(M)]
    sums = [max(new_sums[j], new_sums[j+1]) for j in range(M-1)]
print((List[0][0]+sums[0]))
