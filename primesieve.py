
import os
import inflect
inf = inflect.engine()

import time
start_time = time.time()

def prime_sieve0(bound):
    L = []
    N = [True]*bound
    N[0] = N[1] = False
    for (i, isprime) in enumerate(N):
        if isprime:
            L.append(i)
            for j in range(i**2, bound, i):
                N[j] = False
    return L

def prime_sieve1(bound):
    N = [True]*bound
    N[0] = N[1] = False
    for (i, isprime) in enumerate(N):
        if isprime:
            yield i
            for j in range(i**2, bound, i):
                N[j] = False

for p in sieve(1000000):
    if len(''.join([''.join([i for i in inf.number_to_words(p) if i.isalpha()])])) == p - 1:
        print(p)

exectime = time.time() - start_time
os.chdir(os.path.expanduser("~") + "/src/project-euler")
timesfile = open("exectimes.txt", 'w')
timesfile.write(str(exectime) + " seconds\n")
