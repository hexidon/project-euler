'''def sieve(limit):
    a = [True] * limit                          # Initialize the primality list
    a[0] = a[1] = False

    for (i, isprime) in enumerate(a):
        if isprime:
            yield i
            for n in xrange(i*i, limit, i):     # Mark factors non-prime
                a[n] = False
'''
import os
import time
start_time = time.time()

def sieve(limit):
    L = []
    N = [True]*limit
    N[0] = N[1] = False
    for (i, isprime) in enumerate(N):
        if isprime:
            L.append(i)
            for j in xrange(i**2, limit, i):
                N[j] = False
    return L
            
print sum([i for i in sieve(2000000)])

exectime = time.time() - start_time

os.chdir(os.path.expanduser("~") + "/pe/")
timesfile = open("exectimes.txt", w)
timesfile.write("Problem 10 took %s seconds\n" % exectimes)
