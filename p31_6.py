# Project Euler problem 31 (https://projecteuler.net/problem=31) by vks 

L = [200, 100, 50, 20, 10, 5, 2, 1]
final_count = 1 #we begin by counting 200 itself
M = [100] #every other iteration, M becomes a new way to sum to 200
index_M = 0 #index_M keeps track of the last element of M which is not 1, in order to "backtrack"
index_L = 1 #index_L keeps track of the element of L with which to tweak M

while M[0] != 1:
    adds_to_200 = False
    while not adds_to_200:
        if sum([x for x in M]) + L[index_L] <= 200:
            M.append(L[index_L])
            if L[index_L] != 1:
                index_M += 1
        else:
            index_L += 1
        if sum([x for x in M]) == 200:
            adds_to_200 = True
    if adds_to_200:
        index_L = L.index(M[index_M]) + 1
        M[index_M] = L[index_L]
        M = M[:(index_M+1)]
        if M[index_M] == 1:
            index_M -= 1
        final_count += 1
    if M[0] == 1:
        final_count += 1
print(final_count)
