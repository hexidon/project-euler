#include <stdio.h>

int main(){
  int sunday_count = 0, day = 0; // 1 Jan 1900 was a Monday
  int year = 1900;
  int i, j;
  const char * days[7];
  days[0] = "Monday";
  days[1] = "Tuesday";
  days[2] = "Wednesday";
  days[3] = "Thursday";
  days[4] = "Friday";
  days[5] = "Saturday";
  days[6] = "Sunday";
  const char * months[12];
  months[0] = "January";
  months[1] = "February";
  months[2] = "March";
  months[3] = "April";
  months[4] = "May";
  months[5] = "June";
  months[6] = "July";
  months[7] = "August";
  months[8] = "September";
  months[9] = "October";
  months[10] = "November";
  months[11] = "December";

  int month_count[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
  int leap_year_count[12];
  for (i=0; i<12; i++){
    leap_year_count[i] = month_count[i];
  }
  leap_year_count[1] = 29;
  while (year < 2001){
    if ((year%4 != 0) || (year%100 != 0 && year%400 == 0)){
      for (i=0; i<12; i++){
	for (j=0; j<month_count[i]; j++){
	  if (j == 0 && day==6){
	    printf("%s %d\n", months[i], year);
	    printf("%2d: %s\n", j+1, days[day]);
	    sunday_count ++;
	  }
	  day = (day+1) % 7;
	}
      }
    }
    else{
    //    else if ((year%4==0)&&(year%100!=0||year%400==0)) {
      for (i=0; i<12; i++){
	for (j=0; j<leap_year_count[i]; j++){
	  if (j==0 && day==6){
	    printf("%s %d\n", months[i], year);
	    printf("%2d: %s\n", j+1, days[day]);
	    sunday_count ++;
	  }
	  day = (day+1) % 7;
	}
      }
    }
    year++;
  }
  printf("%d\n", sunday_count);
}

