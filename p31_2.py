def tri(n):
    answer=0
    for i in xrange(n+1):
        for j in xrange(i):
            answer+=1
    return answer

for i in xrange(50):
    print tri(i)
    print i*(i+1)/2


def tri_sq(n):
    return sum([i**2 for i in xrange(n+1)])

for i in xrange(50):
    print tri_sq(i)
    print i*(i+1)*(2*i+1)/6
