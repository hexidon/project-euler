pandigital_products = []
Sum = 0

for n in range(1, 101):
    for m in range(n, 10001):
        if ( n*m not in pandigital_products ) and ( sorted([int(i) for i in str(n)] + [int(i) for i in str(m)] + [int(i) for i in str(n*m)]) == sorted(range(1, 10)) ):
            pandigital_products.append(n*m)
            print(str(n)+" x "+str(m)+" = "+str(n*m))
            Sum += n*m
            
print(Sum)
