from math import *

Coins = [1, 2, 5, 10, 20, 50, 100, 200]
coinCombinations = [[1]*100]
i = 0

while coinCombinations[i] != [200]:
    j = 1
    sum_to_j = sum([coinCombinations[k] for k in range(0, j+1)])
    while sum_to_j not in Coins:
        j += 1
    coinCombinations.append([sum_to_j] + [(j+1):])
    i += 1
coinCombinations.append([200])

print len(coinCombinations)
