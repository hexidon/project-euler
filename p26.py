from math import *
from decimal import *
from factors import *
getcontext().prec=10000
fracs={}

def recDig(n):
    lol=str(n)[int(floor(2-log(n)/log(10))):]
    for i in range(1, int(floor(len(lol)/2.0))+1):
        check=lol[:i]
        for j in range(0, len(lol)-len(lol)%i, i):
            if lol[j:j+i] != check:
                check = -1
                break
            else:
                check=lol[j:j+i]
        if check != -1:
            return check

for d in range(500,1001): #if L=largest rec. cycle: len(L)=len(L*2)==>L>1000/2
    if prime(d):
        fracs[recDig(Decimal(1)/Decimal(d))] = d
biggest=[0, 0]
print fracs.get(str(max([int(i) for i,j in fracs.iteritems()])))
