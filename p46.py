from math import *
counterexample = 0
N = 9

def prime(n): #slow check
    for i in range(2, int(floor(sqrt(n)))+1):
        if (n%i == 0):
            return False
    if n>1:
        return True
    else:
        return False
    
def prime_sieve(bound):
    N = [True]*bound
    N[0] = N[1] = False
    for (i, isprime) in enumerate(N):
        if isprime:
            yield i
            for j in range(i**2, bound, i):
                N[j] = False

while counterexample == 0:
    if not prime(N):
        check = 0
        for i in prime_sieve(N):
            if sqrt((N-i)/2)%1 == 0.0:
                check = 1
#                print(N, " = ", i, " + "  
                break
        if check == 0:
            print(N)
    N += 2
        
print(counterexample)
