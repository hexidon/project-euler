# Project Euler problem 31 (https://projecteuler.net/problem=31) by vks 

L = [200, 100, 50, 20, 10, 5, 2, 1]
final_count = 0
index_M = 0
index_L = 1
M = [100]

while M[0] != 1:
    print(M, index_L, index_M)
    adds_to_200 = False
    while not adds_to_200:
        if sum([x for x in M]) + L[index_L] <= 200:
            M.append(L[index_L])
            if sum([x for x in M]) == 200:
                final_count += 1
                index_L += 1
                adds_to_200 = True
            if L[index_L] != 1:
                index_M += 1
        else:
            index_L += 1
            M.append(L[index_L])
    tail_all_ones = True
    for x in M[(index_M+1):]:
        if x != 1:
            tail_all_ones = False
    if sum([x for x in M]) == 200:
        if not tail_all_ones:
            index_L += 1
            M[index_M] = L[index_L]
        elif tail_all_ones:
            M = M[:(index_M+1)]
            index_L = L.index(M[index_M])
            M[index_M] = L[index_L]

print(final_count)
