import itertools
primesto17 = [2, 3, 5, 7, 11, 13, 17]
Sum = 0

for s in itertools.permutations('0123456789', 10):
    if s[0] != 0:
        pan = "".join(s)
        for i in range(0, 7):
            if (int(pan[i+1])*100 + int(pan[i+2])*10 + int(pan[i+3])) % primesto17[i] != 0:
                break
        else:
            Sum += int(pan)
print Sum
