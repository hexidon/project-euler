/* Project Euler, Problem 43 (https://projecteuler.net/problem=43) by vks */

#include <stdio.h>
#include <math.h>
#include <time.h>

void reversed(int * List, int x, int y);
int permute(int * List, int length);

int main(){
  clock_t begin = clock();
  int final_perm = 0, length = 10;
  int primes_to_17[] = {2, 3, 5, 7, 11, 13, 17};
  int pan[] = {1,0,2,3,4,5,6,7,8,9}; // starting here to eliminate permutations starting with 0
  long int Sum = 0;
  
  while (permute(pan, length) == 0){
    int k;
    permute(pan, length);
    int check = 1;
    for (k = 0; k < 7; k++){
      if ((pan[k+1]*100 + pan[k+2]*10 + pan[k+3]) % primes_to_17[k] != 0){
	check = 0;
      }
    }
    if (check == 1){
      for (k=0; k<length; k++){
	Sum += (long)(pan[k] * pow(10, (long)(9-k)));
      }
    }
  }
  printf("%ld\n", Sum);
  clock_t end = clock();
  double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
  printf("%f\n", time_spent);
  return 0;
}


void reversed(int * List, int x, int y){
  int l;
  int Lizst[y-x+1];
  for (l = x; l <= y; l++){
    Lizst[l-x] = List[y-(l-x)];
  }
  for (l = x; l <= y; l++){
    List[l] = Lizst[l-x];
  }
}

int permute(int * List, int len){
  int i, j, k;
  int permuted[len];
  for (i = len-1; i >= 1 && List[i-1] >= List[i]; i--)
    ;
  if (i == 0){
    return -1;
  }
  for (j = len-1; j > i-1 && List[i-1] >= List[j]; j--)
    ;
  for (k = 0; k < i-1; k++){
    permuted[k] = List[k];
  }
  permuted[i-1] = List[j];
  permuted[j] = List[i-1];
  reversed(permuted, i, len-1);
  for (k = 0; k < len; k++){
    List[k] = permuted[k];
  }
  return 0;
}
