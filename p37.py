import os
os.chdir(os.path.expanduser('~') + '/src/project-euler')
from primesieve import sieve
import factors

count = 0
while count < 11:
    for p in sieve(1000000): #the upper bound was a wild guess--turned out to work.
        if len(str(p)) > 1:
            for t in range(len(str(p))):
                if (not factors.prime(int(str(p)[:(len(str(p))-t)]))) or (not factors.prime(int(str(p)[t:]))):
                    break
            else:
                count += p
                print(p)

print(count)
            
        

