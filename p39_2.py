import inflect
inf = inflect.engine()
print(len(''.join([''.join([i for i in inf.number_to_words(N) if i.isalpha()]) for N in range(1, 1001)])))
