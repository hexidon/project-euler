/* Project Euler problem 67 */

#include <stdio.h>
#include <math.h>

int main() {
  int c, i = 0, j = 0, n = 0, N = 100, M = 100;
  int Tree[N][M];  //since we already know there are 100 rows
  while ((c=getchar()) != EOF) {
    if (c == '\n') {
      Tree[i][j] = n;
      i++;
      n = 0;
      j = 0;
    }
    else if (c == ' ') {
      Tree[i][j] = n;
      n = 0;
      j++;
    }
    else {
      n = 10*n + (c - '0');
    }
  }
  for (i = 0; i < 100; i++) {
    for (j = 0; j < 100; j++) {
      printf("%d ", Tree[i][j]);
    }
    printf("\n");
  }
  int Sums[N-1];
  for (j = 0; j < M-1; j++){
    Sums[j] = Max(Tree[N-1][j], Tree[N-1][j+1]);
  }
  for (i = N-2; i > 0; i--) {
    
    int New_sums[M];
    for (j = 0; j < M; j++) {
      New_sums[j] = Tree[i][j] + Sums[j];
    }
    for (j = 0; j < M-1; j++) {
      Sums[j] = Max(New_sums[j], New_sums[j+1]);
    }
    M--;
  }
  printf("%d\n", Tree[0][0] + Sums[0]); 
  return 0;
}

int Max(int n, int m) {
  if (n > m) {
    return n;
  }
  else {
    return m;
  }
}
