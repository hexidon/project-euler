# DOESN'T WORK

coins = [1, 2, 5, 10, 20, 50, 100, 200]
amount = 200
memo = [[0]*(len(coins)-1)]*amount
def ways(target, avc):
    if avc <= 1: return 1
    t = target
    if memo[t][avc-2] > 0:
        return memo[t][avc]
    res = 0
    while target >= 0:
        res += ways(target, avc-1)
        target -= coins[avc-2]
    memo[t][avc] = res
    return res
print ways(amount, 8)
