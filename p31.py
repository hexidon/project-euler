from math import *

#1, 2, 5, 10, 20, 50, 100 or 200 p
def tri(n):
    return n*(n+1)/2

def comb(m, n):
    M = min(m, n)
    N = max(m, n)
    return tri(N) - tri(N-M)

def tri_n(n, count): 
    while count > 0:
        n = tri(n)+1 #we need to include the coin itself hence +1
        count -= 1
    return n

#checking if shit works
exp5 = tri(2)+1
exp10 = tri(exp5)+1
exp20 = tri(exp10)+1
exp40 = tri(exp20)
print exp40
print tri_n(4, 3)

exp50 = exp40*exp10 + 1
print exp50
exp100 = tri(exp50) + 1
exp200 = tri(exp100) + 1
print exp200

exp200 =  tri_n(comb(tri_n(4, 3), 10), 2)
print exp200
