/* Project Euler, Problem 43. */

#include <stdio.h>

int * permute(int n[], int length);
int power(int n, int m);
int permuted[10];

int main(){
  int i;
  int primes_to_17[] = {2, 3, 5, 7, 11, 13, 17};
  int pan[] = {1,0,2,3,4,5,6,7,8,9}; // starting at 102..9 to eliminate permutations starting with 0
  int s[10];
  long int Sum = 0;
  
  while (s[0] != -1){
    int i;
    int newpan[10];
    for (i = 0; i < 10; i++){
      newpan[i] = permute(pan, 10)[i];
    }
    for (i = 0; i < 10; i++){
      pan[i] = newpan[i];
    }
    for (i = 0; i < 7; i++){
      if ((pan[i+1]*100 + pan[i+2]*10 + pan[i+3]) % primes_to_17[i] != 0){
	break;
      }
    }
    for (i=0; i<10; i++){
      Sum += pan[i] * power(10, 9-i);
    }
  }
  printf("%ld", Sum);
}

int * permute(int * List, int length){ 
  int i, j, k;
  extern int permuted[length];
  for (i = length; List[i-1] >= List[i]; i--){
    ;
  }
  if (i == 0){
    for (j=0; j<length; j++){
      permuted[j] = -1;
      return permuted;
    }
  }
  for (j = length; List[i-1] >= List[j]; j--){
    ;
  }
  for (k = 0; k < length; k++){
    permuted[k] = List[k];
  }
  permuted[i] = List[j];
  permuted[j] = List[i];
  return permuted;
}

int power(int n, int m){
  int i, p=1;
  for (i=0; i<m; i++){
    p *= n;
  }
  return p;
}
